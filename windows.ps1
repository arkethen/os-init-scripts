$internet = "Mozilla.Firefox", "LibreWolf.LibreWolf", "qBittorrent.qBittorrent", "Microsoft.Teams", "ZeroTier.ZeroTierOne", "Safing.Portmaster"
$media = "VideoLAN.VLC", "Stremio.Stremio"
$games = "HeroicGamesLauncher.HeroicGamesLauncher", "Valve.Steam", "ATLauncher.ATLauncher"
$system = "CrystalDewWorld.CrystalDiskMark", "CrystalDewWorld.CrystalDiskInfo", "BleachBit.BleachBit", "WinFsp.WinFsp"
$social = "Discord.Discord", "Telegram.TelegramDesktop", "OpenWhisperSystems.Signal", "Viber.Viber"
$productivity = "ONLYOFFICE.DesktopEditors", "Streetwriters.Notesnook"
$development = "Microsoft.VisualStudioCode", "Git.Git", "OpenJS.NodeJS.LTS", "Python.Python.3", "JetBrains.Toolbox", "GitHub.GitHubDesktop", "MongoDB.Compass.Full", "GoLang.Go"
$others = "Bitwarden.Bitwarden", "7zip.7zip", "Oracle.VirtualBox", "szTheory.exifcleaner", "Iterate.Cyberduck", "Cryptomator.Cryptomator", "IDRIX.VeraCrypt", "voidtools.Everything", "SharkLabs.ClownfishVoiceChanger", "Voicemod.Voicemod"

$apps = $internet + $media + $system + $social + $productivity + $development + $games + $others

Write-host "Checking app list..."
Foreach ($app in $apps) {
    $listApp = winget list --exact -q $app
    if (![String]::Join("", $listApp).Contains($app)) {
        Write-host "Installing:" $app
        winget install --exact --silent $app
    }
    else {
        Write-host "Skipping install of" $app
    }
}

