#!/bin/bash

function print_header {
echo -e "\\n----\\n-------- $1\\n----\\n"
}

function apt_install {
sudo apt-get install $* -y
}

function flatpak_install {
flatpak install flathub $* --noninteractive -y
}

function snap_install {
sudo snap install $*
}

function python_install {
python3 -m pip install -U $*
}

function download_file {
wget -c $1 -O $2 -v
}

# parameters: link, filename
function install_web_deb {
download_file $1 $2 && apt_install ./$2 && rm ./$2
}

function extract_web_tar {
download_file $1 $2
mkdir $3
tar -xf $2 -C $3 --strip-components $4 && rm $2
}

# ---------- ---------- ----------

echo "
# CUSTOM
export PATH=\$PATH:\$HOME/.local/bin
export PATH=\$PATH:\$HOME/dev/flutter/bin
export PATH=\$PATH:\$HOME/dev/go/bin
export PATH=\$PATH:\$HOME/dev/nvim/bin
export PATH=\$PATH:\$HOME/dev/node/bin

export GO_ROOT=\$HOME/apps/go
export ANDROID_HOME=\$HOME/Android/Sdk/
" >> $HOME/.bashrc
source $HOME/.bashrc

print_header "UPDATING SYSTEM"
sudo apt-get update && sudo apt-get upgrade -y

print_header "APT"
# ERR exfat-fuse exfat-utils
apt_install git bleachbit p7zip p7zip-full p7zip-rar sqlite3 aria2 \
fonts-firacode gocryptfs celluloid sirikali scrcpy rsync ffmpeg net-tools samba virtualbox openjdk-17-jdk

print_header "FLATPAK"
# GNOME gnome-software-plugin-flatpak
# PLASMA plasma-discover-backend-flatpak
apt_install flatpak gnome-software-plugin-flatpak && \
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo && \
flatpak_install com.github.tchx84.Flatseal \
org.gtkhash.gtkhash org.qbittorrent.qBittorrent io.github.peazip.PeaZip com.github.qarmin.czkawka org.x.Warpinator com.bitwarden.desktop \
org.gnome.Lollypop com.stremio.Stremio org.videolan.VLC fr.handbrake.ghb \
org.mozilla.firefox org.kde.falkon io.gitlab.librewolf-community com.microsoft.Edge \
com.discordapp.Discord org.signal.Signal org.telegram.desktop com.viber.Viber chat.delta.desktop \
org.onlyoffice.desktopeditors com.notesnook.Notesnook com.calibre_ebook.calibre com.github.jeromerobert.pdfarranger \
io.github.shiftey.Desktop org.sqlitebrowser.sqlitebrowser com.mongodb.Compass

print_header "SNAP"
snap_install zerotier

print_header "PYTHON PIP"
apt_install python3 python3-pip && \
python_install yt-dlp

# ---------- ---------- ----------

print_header "DEV"

apt_install curl wget

mkdir $HOME/dev
mkdir dev-dl
cd dev-dl

# https://www.rust-lang.org/tools/install
print_header "RUST"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
source "$HOME/.cargo/env"

# https://docs.flutter.dev/get-started/install/linux
print_header "FLUTTER"
git clone https://github.com/flutter/flutter.git -b stable

# https://go.dev/dl/
print_header "GO"
extract_web_tar "https://go.dev/dl/go1.19.4.linux-amd64.tar.gz" go.tar.gz go 1

# https://nodejs.org/en/
print_header "NODE"
extract_web_tar "https://nodejs.org/dist/v18.12.1/node-v18.12.1-linux-x64.tar.xz" node.tar.xz node 1

# https://github.com/neovim/neovim/releases/tag/stable
print_header "NEOVIM"
extract_web_tar "https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.tar.gz" nvim.tar.gz nvim 1

mv * $HOME/dev
cd ..

# https://github.com/alacritty/alacritty
print_header "ALACRITTY"
# install dependencies
apt_install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
# install
cargo install alacritty
# desktop file
download_file "https://github.com/alacritty/alacritty/releases/download/v0.11.0/Alacritty.desktop" alacritty.desktop
mkdir -p $HOME/.local/share/applications
mv alacritty.desktop $HOME/.local/share/applications
# update terminal
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator $HOME/.cargo/bin/alacritty 50
sudo update-alternatives --config x-terminal-emulator

# https://www.lunarvim.org/docs/installation
bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)
mkdir -p $HOME/.local/share/fonts
curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf
mv *.otf $HOME/.local/share/fonts

cd $HOME/Downloads

# ---------- ---------- ----------

print_header "APPS"

mkdir $HOME/Applications

# https://tutanota.com/#download
print_header "TUTANOTA"
download_file "https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage" tutanota.AppImage
mv tutanota.AppImage $HOME/Applications

# https://github.com/szTheory/exifcleaner/releases
print_header "EXIF CLEANER"
download_file "https://github.com/szTheory/exifcleaner/releases/download/v3.6.0/ExifCleaner-3.6.0.AppImage" exif-cleaner.AppImage
mv exif-cleaner.AppImage $HOME/Applications

# https://rclone.org/downloads/
print_header "RCLONE"
curl https://rclone.org/install.sh | sudo bash

# https://code.visualstudio.com/
print_header "VSCODE"
install_web_deb "https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64" vscode.deb

# https://safing.io/
print_header "PORTMASTER"
install_web_deb "https://updates.safing.io/latest/linux_amd64/packages/portmaster-installer.deb" portmaster.deb

# https://www.jetbrains.com/toolbox-app/
print_header "JETBRAINS TOOLBOX"
extract_web_tar "https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-1.27.2.13801.tar.gz" jetbrains-toolbox.tar.gz jetbrains-toolbox 1

# https://github.com/TheAssassin/AppImageLauncher/releases
print_header "APPIMAGE LAUNCHER"
install_web_deb "https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb" appimage-launcher.deb

# https://github.com/BambooEngine/ibus-bamboo
print_header "BAMBOO"
apt_install ibus im-config
sudo add-apt-repository ppa:bamboo-engine/ibus-bamboo
sudo apt-get update
sudo apt-get install ibus ibus-bamboo --install-recommends
im-config -n ibus
ibus-daemon -d
env DCONF_PROFILE=ibus dconf write /desktop/ibus/general/preload-engines "['BambooUs', 'Bamboo']" && \
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us'), ('ibus', 'Bamboo')]"

# ---------- ---------- ----------

print_header "ADDONS"
apt_install ubuntu-restricted-addons ubuntu-restricted-extras -y

print_header "DONE"
